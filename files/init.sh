#!/bin/bash -l
#
# Container entry point and initialization script
#

# Set timezone
if [ ! -z "${TZ}" -a -e "/usr/share/zoneinfo/${TZ}" ]; then
	cp /usr/share/zoneinfo/${TZ} /etc/localtime
else
	cp /usr/share/zoneinfo/UTC /etc/localtime
fi

# Add 'dockerhost' to /etc/hosts
echo -e "$(ip route | awk '/default/ {print $3}')\tdockerhost" >> /etc/hosts

# Run enabled services at startup
for service in ${ENABLE_SERVICES}; do
	/usr/local/bin/service "${service}" start
	rc=$?
	if [ "${EXIT_ON_SERVICE_FAILURE}" == "true" ]; then
		if [ $rc -ne 0 ]; then
			echo
			echo "[FATAL] Failed to start service: ${service}"
			echo
			exit 99
		fi
	fi
done

# Run the command
if [ ! -z "${CONTAINER_NAME}" -a -x "/init-${CONTAINER_NAME}.sh" ]; then
	exec "/init-${CONTAINER_NAME}.sh" "$@"
elif [ -x "/init-${IMAGE_NAME}.sh" ]; then
	exec "/init-${IMAGE_NAME}.sh" "$@"
else
	exec "$@"
fi
