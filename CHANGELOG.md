# Changelog

## Unreleased

- No unreleased changes

## 3.7-r4 (Released 2018-08-02)

- Fix: Fixed error with timezone in /init.sh

## 3.7-r3 (Released 2018-07-30)

- Enhancement: Container initialization can be aborted if a service fails by using **EXIT_ON_SERVICE_FAILURE**
- Change: `/etc/default/...` files will no longer be read at startup
- Change: Services are now started at boot through the use of the **ENABLE_SERVICES** environment variable

## 3.7-r2 (Released 2018-07-28)

- Note: Updated to work with new cibuilder image

## 3.7-r1 (Released 2018-06-19)

- Fix: Changed ENTRYPOINT to spawn /init.sh so it is no longer required for CMD

## 3.7-r0 (Released 2018-06-18)

- Note: Initial release of the container image
- Note: Based on Alpine Linux version 3.7
- Note: Installed dumb-init version 1.2.1
