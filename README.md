# Alpine Linux Base Image

[![pipeline status](https://gitlab.com/imperva-community/public/docker/alpine/badges/3.7/build.svg)](https://gitlab.com/imperva-community/public/docker/alpine/pipelines)
[![license](https://img.shields.io/badge/license-imperva--community-blue.svg)](https://gitlab.com/imperva-community/public/docker/alpine/blob/3.7/LICENSE.md)
[![support](https://img.shields.io/badge/support-community-blue.svg)](https://gitlab.com/imperva-community)

## Description

This is a base image of Alpine Linux derived from the official containers and which is used as the base image for other container projects.

## Startup and Environment

When this image starts, it launches `/usr/local/sbin/dumb-init` as a minimal init system (see links at the bottom for details on **dumb-init**).  The init system will run the `/init.sh` script, which is in charge of setting up the environment and starting an extra services required by the container before finally starting a Bash login shell by default.

Other Docker images based off of this image can replace launching a bash login shell by creating an `/init-IMAGE_NAME.sh` script where **IMAGE_NAME** corresponds to the value of that environment variable.  Typically the variable would be defined in the the **Dockerfile** itself.  Individual containers can also override the default behavior by creating an `/init-CONTAINER_NAME.sh` script where **CONTAINER_NAME** corresponds to the value of that environment variable.

In summary:

- `/usr/local/sbin/dumb-init` launches the `/init.sh` script in the image.
- `/init.sh` sets the time zone for the container (see **Setting the Time Zone** below).
- `/init.sh` adds an entry for **dockerhost** to `/etc/hosts` which points to the IP address on the container's network of the host on which the container is running
- `/init.sh` launches any services which should be started up at boot (see **Services** section below).
- `/init.sh` then replaces itself by using the Bash `exec` builtin with the following precedence:
  - launch `/init-CONTAINER_NAME.sh` if it exists
  - if not, launch `/init-IMAGE_NAME.sh` if it exists
  - if not, launch the command provided by **CMD** in the Dockerfile.

## Setting the Time Zone

A container's timezone can be changed from the default UTC timezone to a local timezone by setting the **TZ** environment variable to a standard Linux timezone (eg: **America/New_York**, **PDT**, etc.).  If the variable is not defined, the timezone is automatically set to UTC.

## Services

Service configuration allows you to automatically start additional services when the container is booted from the `/init.sh` startup script.  By default, the services specified by the **ENABLE_SERVICES** environment variable will be started in the order specified when the container is started.  If the variable is empty, no services will be started automatically.

The services included in this image are:

- **automount** for automatically mounting external filesystems such as CIFS and NFS
- **crond** for running jobs periodically
- **syslog-ng** for sending syslog messages to an external location

You can include additional service scripts by mounting a local volume under `/etc/init.d/services.d` and placing service startup scripts in that folder.

Service scripts can be placed in either `/etc/init.d/services` or `/etc/init.d/services.d`.  The former is intended for use when building container images and adding services.  They are considered *system* services.  The latter is intended to allow for additional startup scripts to be run for an individual container.  The folder should be a volume that is mapped back to the container host.

You can use the `service <service> [action]` command to run actions on services or use `service --list` to list out all services available.  Each service can have its own list of valid actions, however, every service _must_ accept `start` as an action but _should_ accept `stop`, `restart` and `status` actions as well.  It is recommended that you follow the same basic pattern as the `/etc/init.d/services/crond` startup script built into the image.

Finally, you can abort container startup if a service fails to start (by returning a non-zero exit code from its startup script) by setting the **EXIT_ON_SERVICE_FAILURE** variable to `true`.

### Mounting NFS/CIFS File Systems at Startup

This container contains NFS and CIFS utilities for mounting remote file systems. To mount additional file shares, you should place configuration files (see below) in the **/etc/automount/conf.d** folder and you must enable the **automount** service in the container or your derived image.

The following example mounts a CIFS share using a credentials file stored in the **/etc/automount/creds.d** folder:

```mount.d
TYPE=cifs
REMOTE_PATH=//windows_server.domain/share_name/path
MOUNT_POINT=/mnt/share_name
OPTIONS=credentials=/etc/automount/creds.d/share_credentials,rw
```

For information on CIFS credentials files, please review the man page for **mount.cifs**.

The following example mounts an NFS share:

```mount.d
TYPE=nfs
REMOTE_PATH=192.168.1.1:/path/to/folder
MOUNT_POINT=/mnt/share_name
OPTIONS=rw
```

**NOTE:** You must start the container in privileged mode if you wish to mount file systems.

## syslog-ng Integration

**syslog-ng** and **syslog-ng-json** are included in this image in order to provide robust logging capabilities out of the box.

Configuration files for **syslog-ng** should be stored in `/etc/syslog-ng/conf.d`, which should be a volume mapped back to the container host.

By default syslog-ng is not configured to log anything anywhere.  The **syslog-ng-json** module is so that you can log JSON-based messages as well.

## Additional Help or Questions

If you have questions, find bugs or need additional help, please send an email to:
[gsa-team@imperva.com](mailto:gsa-team@imperva.com).

## Links

- [Yelp dumb-init](https://github.com/Yelp/dumb-init)
- [syslog-ng](https://syslog-ng.org)
