#
# Dockerfile for Alpine Linux base container
#
FROM alpine:3.7

ARG DUMB_INIT_VERSION=1.2.1
ARG DUMB_INIT_URL=https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}

# Install packages
#   Add the edge repo and do any package upgrades
#   Add core packages and syslog packages
#   Add local utilities
#   Cleanup
RUN set -xe \
	&& echo @edge http://dl-cdn.alpinelinux.org/alpine/edge/main | tee -a /etc/apk/repositories \
	&& apk update \
	&& apk upgrade \
	&& apk add --update curl bash file su-exec ca-certificates coreutils libcap tzdata xz openssl jq unzip \
		syslog-ng@edge syslog-ng-json@edge nfs-utils cifs-utils \
	&& apk add --virtual build-deps gcc curl-dev python-dev musl-dev openssl-dev linux-headers py-pip \
	&& pip install --upgrade --no-cache-dir pip certifi pycurl netifaces \
	&& apk del build-deps \
	&& mkdir -p /usr/local/bin /usr/local/sbin \
	&& curl -L ${DUMB_INIT_URL}/dumb-init_${DUMB_INIT_VERSION}_amd64 -o /usr/local/sbin/dumb-init \
	&& chgrp adm /var/log \
	&& rm -rf /tmp/* /var/tmp/* /usr/local/src /var/cache/apk/* 

# Add files to image
COPY files /

# Configure image
RUN set -xe \
	&& chmod +x /init.sh /usr/local/bin/* /usr/local/sbin/* /etc/init.d/services/* \
	&& chmod 0700 /root \
	&& rm -f /etc/profile.d/color_prompt \
	&& update-ca-certificates \
	&& sed -i -e 's|/bin/ash|/bin/bash|g' /etc/passwd \
	&& mkdir -p /var/spool/syslog-ng \
	&& chgrp -R adm /var/spool/syslog-ng

# Export volumes
VOLUME [ "/etc/init.d/services.d", "/etc/syslog-ng/conf.d", "/etc/automount/conf.d", "/etc/automount/creds.d" ]

# Set entry point
ENTRYPOINT [ "/usr/local/sbin/dumb-init", "--", "/init.sh" ]

# Configure default command
CMD [ "/bin/bash", "-l" ]

# Required build arguments
ARG NAME
ARG VERSION
ARG RELEASE_DATE
ARG GIT_SHA1
ARG TAGS

# Image build metadata
ENV IMAGE_NAME "${NAME}"
ENV IMAGE_VERSION "${VERSION}"
ENV IMAGE_RELEASE_DATE "${RELEASE_DATE}"
LABEL \
	vendor="Imperva, Inc." \
	maintainer="Imperva GSA Team <gsa-team@imperva.com>" \
	com.imperva.image_name="${IMAGE_NAME}" \
	com.imperva.image_version="${IMAGE_VERSION}" \
	com.imperva.image_release_date="${IMAGE_RELEASE_DATE}" \
	com.imperva.image_tags="${TAGS}" \
	com.imperva.commit_id="${GIT_SHA1}"
